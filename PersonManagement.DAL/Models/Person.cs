﻿using System;
using System.
    Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PersonManagement.DAL.Models
{
    public class Person
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [MaxLength(25, ErrorMessage = "{0} maximum length is 25.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "{0} is required.")]
        [MaxLength(50, ErrorMessage = "{0} maximum length is 50.")]
        public string LastName { get; set; }

        [RegularExpression(@"\d{11}", ErrorMessage = "{0} must be 11 digits.")]
        //TODO: PersonalNumber duplicate validation
        [MaxLength(11)]
        public string PersonalNumber { get; set; }

        public DateTime? Birthdate { get; set; }

        public GenderEnum? Gender { get; set; }

        [Column(TypeName = "money")]
        [Range(0, double.MaxValue, ErrorMessage = "{0} cannot be less than 0.")]
        public decimal? Salary { get; set; }
    }

    public enum GenderEnum {
        [Display(Name = "Male")]
        Male,

        [Display(Name = "Female")]
        Female
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PersonManagement.DAL.Interfaces
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Expression<Func<T, bool>> expression);
        Task<List<T>> ListAsync();
        Task<List<T>> ListAsync(Expression<Func<T, bool>> expression);
        Task<T> GetByIdAsync(int Id);
        Task CreateAsync(T entity);
        void Update(T entity);
        Task DeleteByIdAsync(int Id);
        Task SaveAsync();
    }
}

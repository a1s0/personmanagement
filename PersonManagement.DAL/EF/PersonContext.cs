﻿using Microsoft.EntityFrameworkCore;
using PersonManagement.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PersonManagement.DAL.EF
{
    public class PersonContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        public PersonContext(DbContextOptions<PersonContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>()
                .ToTable("Persons", "Person")
                .HasData(
                PersonContextSeedDataInitializer.InitData()
                );
        }
    }

    internal class PersonContextSeedDataInitializer
    {
        internal static Person[] InitData()
        {
            List<Person> Persons = new List<Person>();
            for (int i = 1; i <= 100; i++)
            {
                Persons.Add(new Person
                {
                    Id = i,
                    FirstName = $"John{i}",
                    LastName = $"Doe{i}"
                });
            }

            return Persons.ToArray();
        }
    }
}

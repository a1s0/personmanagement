﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PersonManagement.DAL.Migrations
{
    public partial class Person_Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Person");

            migrationBuilder.CreateTable(
                name: "Persons",
                schema: "Person",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(maxLength: 25, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false),
                    PersonalNumber = table.Column<string>(maxLength: 11, nullable: true),
                    Birthdate = table.Column<DateTime>(nullable: true),
                    Gender = table.Column<int>(nullable: true),
                    Salary = table.Column<decimal>(type: "money", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                });

            migrationBuilder.InsertData(
                schema: "Person",
                table: "Persons",
                columns: new[] { "Id", "Birthdate", "FirstName", "Gender", "LastName", "PersonalNumber", "Salary" },
                values: new object[,]
                {
                    { 1, null, "John1", null, "Doe1", null, 0m },
                    { 73, null, "John73", null, "Doe73", null, 0m },
                    { 72, null, "John72", null, "Doe72", null, 0m },
                    { 71, null, "John71", null, "Doe71", null, 0m },
                    { 70, null, "John70", null, "Doe70", null, 0m },
                    { 69, null, "John69", null, "Doe69", null, 0m },
                    { 68, null, "John68", null, "Doe68", null, 0m },
                    { 67, null, "John67", null, "Doe67", null, 0m },
                    { 66, null, "John66", null, "Doe66", null, 0m },
                    { 65, null, "John65", null, "Doe65", null, 0m },
                    { 64, null, "John64", null, "Doe64", null, 0m },
                    { 63, null, "John63", null, "Doe63", null, 0m },
                    { 62, null, "John62", null, "Doe62", null, 0m },
                    { 61, null, "John61", null, "Doe61", null, 0m },
                    { 60, null, "John60", null, "Doe60", null, 0m },
                    { 59, null, "John59", null, "Doe59", null, 0m },
                    { 58, null, "John58", null, "Doe58", null, 0m },
                    { 57, null, "John57", null, "Doe57", null, 0m },
                    { 56, null, "John56", null, "Doe56", null, 0m },
                    { 55, null, "John55", null, "Doe55", null, 0m },
                    { 54, null, "John54", null, "Doe54", null, 0m },
                    { 53, null, "John53", null, "Doe53", null, 0m },
                    { 74, null, "John74", null, "Doe74", null, 0m },
                    { 52, null, "John52", null, "Doe52", null, 0m },
                    { 75, null, "John75", null, "Doe75", null, 0m },
                    { 77, null, "John77", null, "Doe77", null, 0m },
                    { 98, null, "John98", null, "Doe98", null, 0m },
                    { 97, null, "John97", null, "Doe97", null, 0m },
                    { 96, null, "John96", null, "Doe96", null, 0m },
                    { 95, null, "John95", null, "Doe95", null, 0m },
                    { 94, null, "John94", null, "Doe94", null, 0m },
                    { 93, null, "John93", null, "Doe93", null, 0m },
                    { 92, null, "John92", null, "Doe92", null, 0m },
                    { 91, null, "John91", null, "Doe91", null, 0m },
                    { 90, null, "John90", null, "Doe90", null, 0m },
                    { 89, null, "John89", null, "Doe89", null, 0m },
                    { 88, null, "John88", null, "Doe88", null, 0m },
                    { 87, null, "John87", null, "Doe87", null, 0m },
                    { 86, null, "John86", null, "Doe86", null, 0m },
                    { 85, null, "John85", null, "Doe85", null, 0m },
                    { 84, null, "John84", null, "Doe84", null, 0m },
                    { 83, null, "John83", null, "Doe83", null, 0m },
                    { 82, null, "John82", null, "Doe82", null, 0m },
                    { 81, null, "John81", null, "Doe81", null, 0m },
                    { 80, null, "John80", null, "Doe80", null, 0m },
                    { 79, null, "John79", null, "Doe79", null, 0m },
                    { 78, null, "John78", null, "Doe78", null, 0m },
                    { 76, null, "John76", null, "Doe76", null, 0m },
                    { 51, null, "John51", null, "Doe51", null, 0m },
                    { 50, null, "John50", null, "Doe50", null, 0m },
                    { 49, null, "John49", null, "Doe49", null, 0m },
                    { 22, null, "John22", null, "Doe22", null, 0m },
                    { 21, null, "John21", null, "Doe21", null, 0m },
                    { 20, null, "John20", null, "Doe20", null, 0m },
                    { 19, null, "John19", null, "Doe19", null, 0m },
                    { 18, null, "John18", null, "Doe18", null, 0m },
                    { 17, null, "John17", null, "Doe17", null, 0m },
                    { 16, null, "John16", null, "Doe16", null, 0m },
                    { 15, null, "John15", null, "Doe15", null, 0m },
                    { 14, null, "John14", null, "Doe14", null, 0m },
                    { 13, null, "John13", null, "Doe13", null, 0m },
                    { 12, null, "John12", null, "Doe12", null, 0m },
                    { 11, null, "John11", null, "Doe11", null, 0m },
                    { 10, null, "John10", null, "Doe10", null, 0m },
                    { 9, null, "John9", null, "Doe9", null, 0m },
                    { 8, null, "John8", null, "Doe8", null, 0m },
                    { 7, null, "John7", null, "Doe7", null, 0m },
                    { 6, null, "John6", null, "Doe6", null, 0m },
                    { 5, null, "John5", null, "Doe5", null, 0m },
                    { 4, null, "John4", null, "Doe4", null, 0m },
                    { 3, null, "John3", null, "Doe3", null, 0m },
                    { 2, null, "John2", null, "Doe2", null, 0m },
                    { 23, null, "John23", null, "Doe23", null, 0m },
                    { 24, null, "John24", null, "Doe24", null, 0m },
                    { 25, null, "John25", null, "Doe25", null, 0m },
                    { 26, null, "John26", null, "Doe26", null, 0m },
                    { 48, null, "John48", null, "Doe48", null, 0m },
                    { 47, null, "John47", null, "Doe47", null, 0m },
                    { 46, null, "John46", null, "Doe46", null, 0m },
                    { 45, null, "John45", null, "Doe45", null, 0m },
                    { 44, null, "John44", null, "Doe44", null, 0m },
                    { 43, null, "John43", null, "Doe43", null, 0m },
                    { 42, null, "John42", null, "Doe42", null, 0m },
                    { 41, null, "John41", null, "Doe41", null, 0m },
                    { 40, null, "John40", null, "Doe40", null, 0m },
                    { 39, null, "John39", null, "Doe39", null, 0m },
                    { 99, null, "John99", null, "Doe99", null, 0m },
                    { 38, null, "John38", null, "Doe38", null, 0m },
                    { 36, null, "John36", null, "Doe36", null, 0m },
                    { 35, null, "John35", null, "Doe35", null, 0m },
                    { 34, null, "John34", null, "Doe34", null, 0m },
                    { 33, null, "John33", null, "Doe33", null, 0m },
                    { 32, null, "John32", null, "Doe32", null, 0m },
                    { 31, null, "John31", null, "Doe31", null, 0m },
                    { 30, null, "John30", null, "Doe30", null, 0m },
                    { 29, null, "John29", null, "Doe29", null, 0m },
                    { 28, null, "John28", null, "Doe28", null, 0m },
                    { 27, null, "John27", null, "Doe27", null, 0m },
                    { 37, null, "John37", null, "Doe37", null, 0m },
                    { 100, null, "John100", null, "Doe100", null, 0m }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Persons",
                schema: "Person");
        }
    }
}

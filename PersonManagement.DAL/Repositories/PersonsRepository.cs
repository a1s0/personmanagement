﻿using Microsoft.EntityFrameworkCore;
using PersonManagement.DAL.EF;
using PersonManagement.DAL.Interfaces;
using PersonManagement.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PersonManagement.DAL.Repositories
{
    public class PersonsRepository : IRepository<Person>
    {
        private PersonContext db;

        public PersonsRepository(PersonContext ctx)
        {
            db = ctx;
        }

        public IEnumerable<Person> GetAll()
        {
            return db.Persons;
        }

        public IEnumerable<Person> GetAll(Expression<Func<Person, bool>> expression)
        {
            return db.Persons.Where(expression);

        }

        public Task CreateAsync(Person entity)
        {
            return db.AddAsync(entity);
        }

        public async Task DeleteByIdAsync(int Id)
        {
            Person dbPerson = await db.FindAsync<Person>(Id);
            if (null != dbPerson)
            {
                db.Remove(dbPerson);
            }
        }

        public Task<Person> GetByIdAsync(int Id)
        {
            return db.Persons.FindAsync(Id);
        }

        public Task<List<Person>> ListAsync()
        {
            return db.Persons.ToListAsync();
        }

        public Task<List<Person>> ListAsync(Expression<Func<Person, bool>> expression)
        {
            return db.Persons.Where(expression).ToListAsync();
        }

        public void Update(Person entity)
        {
            db.Entry<Person>(entity).State = EntityState.Modified;
        }

        public Task<bool> IsPersonalNumberTakenAsync(string personalNumber)
        {
            if (String.IsNullOrWhiteSpace(personalNumber))
            {
                return Task.FromResult(false);
            }
            else
            {
                return db.Persons.AnyAsync(x => String.Equals(x.PersonalNumber, personalNumber));
            }
        }

        public Task<bool> IsPersonalNumberTakenByAnotherPersonAsync(Person currentPerson)
        {
            if (currentPerson == null)
            {
                throw new ArgumentNullException(nameof(currentPerson));
            }

            if (String.IsNullOrWhiteSpace(currentPerson.PersonalNumber))
            {
                return Task.FromResult(false);
            }
            else
            {
                return db.Persons.AnyAsync(x => !String.IsNullOrWhiteSpace(currentPerson.PersonalNumber)
                && String.Equals(x.PersonalNumber, currentPerson.PersonalNumber)
                && x.Id != currentPerson.Id);
            }
        }

        public Task SaveAsync()
        {
            return db.SaveChangesAsync();
        }
    }
}

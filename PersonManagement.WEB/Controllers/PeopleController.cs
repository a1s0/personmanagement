﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PersonManagement.DAL.EF;
using PersonManagement.DAL.Models;
using PersonManagement.DAL.Repositories;
using PersonManagement.Domain.Infrastructure;
using PersonManagement.WEB.ViewModels;


namespace PersonManagement.WEB.Controllers
{
    public class PeopleController : Controller
    {
        private readonly PersonsRepository _persons;
        private readonly int pagedListPageSize = 10;

        public PeopleController(PersonsRepository personsRepository)
        {
            _persons = personsRepository;
        }

        [HttpGet]
        public async Task<IActionResult> Index(int? page = 1, string searchstring = "")
        {
            var vm = new PersonsListViewModel();
            IEnumerable<Person> dbItems;

            if (String.IsNullOrWhiteSpace(searchstring))
            {
                dbItems = _persons.GetAll();
            }
            else
            {
                dbItems = _persons.GetAll(e => e.FirstName.Contains(searchstring) || e.LastName.Contains(searchstring));
            }

            vm.PaginatedList = new PaginatedList<Person>(dbItems.AsQueryable().AsNoTracking(),(int) page, pagedListPageSize);
            vm.CurrentSearchString = searchstring;
            return View(vm);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var person = await _persons.GetByIdAsync((int)id);
            if (person == null)
            {
                return NotFound();
            }

            return View(person);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        // TODO: change Person to PersonDTO
        public async Task<IActionResult> Edit(Person person)
        {
            if (await _persons.IsPersonalNumberTakenByAnotherPersonAsync(person)) {
                ModelState.AddModelError("Personal number", $"Personal number {person.PersonalNumber} is taken by another person.");
            }

            if (ModelState.IsValid)
            {
                _persons.Update(person);
                await _persons.SaveAsync();
                return RedirectToAction(nameof(Edit), new { id = person.Id });
            }
            return View(person);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FirstName,LastName,PersonalNumber,Birthdate,Gender,Salary")] Person person)
        {
            if (await _persons.IsPersonalNumberTakenAsync(person.PersonalNumber)){
                ModelState.AddModelError("Personal number", $"Personal number {person.PersonalNumber} is already taken.");
            }

            if (ModelState.IsValid)
            {
                await _persons.CreateAsync(person);
                await _persons.SaveAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(person);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(int personId) {

            if (personId == 0) {
                return BadRequest();
            }

            await _persons.DeleteByIdAsync(personId);
            await _persons.SaveAsync();
            return RedirectToAction(nameof(Index));

        }
    }
}

﻿using PersonManagement.DAL.Models;
using PersonManagement.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PersonManagement.WEB.ViewModels
{
    public class PersonsListViewModel
    {
        public PaginatedList<Person> PaginatedList { get; set; }
        public string CurrentSearchString { get; set; }
    }
}

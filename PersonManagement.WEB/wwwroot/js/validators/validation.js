﻿
// number validation override to allow decimals with , and . delimeter
$(document).ready(function () {
    $.validator.methods.number = function (value, element) {
        return this.optional(element) || /^-?\d+(\.|\,)?\d*$/.test(value);
    };
});
﻿$(document).ready(function () {
    initDeleteButtons();
});

function initDeleteButtons() {
    $('.js-delete-button').click(function () {
        deletePerson($(this));
    });
}

function deletePerson(item) {
    let personId = item.attr('data-person-id');
    let token = $('input[name="__RequestVerificationToken"]').val();

    $.ajax(
        {
            method: 'POST',
            url: 'People/Delete',
            data: {
                __RequestVerificationToken: token,
                personId: personId
            },
            success: function () {
                $('tr[data-person-id=' + personId + ']').remove();
            },
            fail: function () {
                // some logging
            }
        }
    );
}